# -*- coding: utf-8 -*-

"""Provides a lazy way to recursively `git status|pull|push` all repos.
Use with caution."""

import argparse
import concurrent.futures
import configparser
import fnmatch
import json
import logging
import multiprocessing
import os
import re
import shlex
import subprocess
import sys
import time

from bashutils import logmsg

# -----------------------------------------------------------------------------


repo_list = []
exclude_list = []


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if "log_time" in kw:
            name = kw.get("log_name", method.__name__.upper())
            kw["log_time"][name] = int((te - ts) * 1000)
        else:
            print("%r  %2.2f ms" % (method.__name__, (te - ts) * 1000))
        return result

    return timed


def get_parser():
    """Returns the Argument Parser."""

    parser = argparse.ArgumentParser(
        description="Does bulk status|push|pull operations on git repos."
    )

    # required arguments
    parser.add_argument(
        "--method", "-m", help="method to call", choices=["status", "push", "pull"]
    )

    # optional arguments
    parser.add_argument("--add", "-a", help="add a repo")
    parser.add_argument("--remove", "-r", help="removed a repo")
    parser.add_argument(
        "--ignore", "-i", help="ignore a repo", default=False, action="store_true"
    )

    parser.add_argument("--find", help="find all repos", action="store_true")
    parser.add_argument("--sync", help="sync all repos", action="store_true")

    parser.add_argument(
        "--update_origins", help="update all repo origins", action="store_true"
    )
    parser.add_argument("--ask_ignore", help="ask to ignore repos", action="store_true")

    return parser


def exec_cmd(cmd):
    """Executes a command and returns its output."""
    try:
        result = subprocess.check_output(shlex.split(cmd), stderr=subprocess.STDOUT)
        result = result.decode("utf-8")
        return result
    except subprocess.CalledProcessError:
        logmsg.error(f'Command failed: "{cmd}" executed in "{os.getcwd()}"')
        return None


def get_dirs(search):
    """Return the dir paths we're interested in."""
    dirs = []
    for mydir in search:
        mydir = os.path.join(os.path.expanduser(mydir), "")
        dirs.append(mydir)

    return dirs


def agnostic_path(dirpath):
    """Return a platform agnostic path we can use on Linux or Windows."""
    # Remove the user dir portion and replace with placeholder
    dirpath = dirpath.replace(os.path.expanduser("~"), "~")
    # Replace any backslashes in the path with a forward slash
    dirpath = dirpath.replace("\\", "/")
    # Remove any trailing slash
    dirpath = dirpath.rstrip("/")
    return dirpath


def platform_path(dirpath):
    """Return a path specific to Linux or Windows."""
    return os.path.abspath(os.path.expanduser(dirpath))


#@timeit
def find_repos(dirpath):
    """Walk directory and find git repo directories."""
    logmsg.header(f"Searching: {dirpath} ...")
    repo_dirs = []
    for root, dirnames, filenames in os.walk(dirpath):
        for dirname in fnmatch.filter(dirnames, ".git"):
            dirpath = os.path.join(root, os.path.dirname(dirname))
            if any(x in dirpath for x in exclude_list):
                print(f'Skipping {dirpath} ...')
                continue
            repo_dirs.append(dirpath)
            # print(dirpath)

    return repo_dirs


#@timeit
def get_repo_list_info(repo_dirs, repo_users=[]):
    """Get repo list info."""
    # Method 1
    # for dirpath in repo_dirs:
    #     method_args
    #     repo_info = get_repo_info(dirpath)
    #     if not repo_info:
    #         logmsg.error(f'Unable to get repo info for "{dirpath}"')
    #         continue

    #     if any(user in repo_info['fetch_url'] for user in repo_users):
    #         if not repo_exists(dirpath):
    #             repo_list.append(repo_info)
    #             # if logmsg.confirm('Would you like to add "%s"' % dirpath):
    #             #     repo_list.append(repo_info)
    #             # else:
    #             #     repo_info['ignore'] = True
    #             #     repo_list.append(repo_info)

    # return repo_list

    # Method 2
    # pool = multiprocessing.Pool(processes=8)
    # pool.map(get_repo_info, repo_dirs)
    # pool.close()
    # pool.join()
    # return

    # Method 3
    # https://medium.com/@ageitgey/quick-tip-speed-up-your-python-data-processing-scripts-with-process-pools-cf275350163a
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for dirpath, repo_info in zip(
            repo_dirs, executor.map(get_repo_info, repo_dirs)
        ):
            # print('-' * 40)
            # print(f'Repo dir: {dirpath}')
            # print(f'Repo info: {repo_info}')
            if repo_info and not repo_exists(dirpath):
                if "vendor" not in repo_info["path"]:
                    print(f'=> Add "{dirpath}"')
                    repo_list.append(repo_info)
                else:
                    logmsg.warning(f'Skipping "{dirpath}"')

    return repo_list


def ask_ignore(repo_users):
    for obj in repo_list:
        dirpath = obj["path"]
        if not obj["ignore"]:
            # If any user is in the fetch_url we are interested in it
            if any(user in obj["fetch_url"] for user in repo_users):
                if logmsg.confirm('Would you like to ignore "%s"' % dirpath):
                    obj["ignore"] = True


def sync_repos():
    """Clone any repos that are not on this system."""
    for obj in repo_list:
        if "ignore" in obj and obj["ignore"] is True:
            continue
        dirpath = platform_path(obj["path"])
        if not os.path.exists(dirpath):
            if logmsg.confirm(f'"{dirpath}" does not exist, create it'):
                os.makedirs(dirpath)
                output = exec_cmd('git clone %s "%s"' % (obj["fetch_url"], dirpath))
                # if output:
                #     print(output.strip())


def write_file(repo_list, filepath):
    """Write repo matches to file."""
    with open(filepath, "w") as f:
        data = json.dumps(repo_list, sort_keys=True, indent=4)
        f.write(data)


def read_file(filepath):
    """Return git_repos file contents as JSON."""
    logmsg.header("Reading repo_lists.json file ...")
    try:
        with open(filepath, "r") as f:
            jsondata = json.loads(f.read())
    except IOError:
        logmsg.warning(f'"{filepath}" not found')
        jsondata = []

    return jsondata


def repo_exists(dirpath):
    """Return if repo exists or not."""
    dirpath = agnostic_path(dirpath)
    for r in repo_list:
        if r["path"] == dirpath:
            return True
    return False


def get_repo_info(dirpath):
    """Get repo URL from given directory."""
    abs_path = platform_path(dirpath)
    os.chdir(abs_path)

    output = exec_cmd(f"git remote show origin")
    if output:
        # Get fetch URL
        regex = r"Fetch URL: (.+)\n"
        matches = re.search(regex, output)
        if matches:
            fetch_url = matches.group(1)
        # Get push URL
        regex = r"Push  URL: (.+)\n"
        matches = re.finditer(regex, output, re.MULTILINE)

        push_urls = []
        for matchNum, match in enumerate(matches):
            matchNum = matchNum + 1
            push_urls.append(match.group(1))

        # Create platform agnostic path
        dirpath = agnostic_path(dirpath)
        # print('-'*40)
        # print(dirpath)
        # print(fetch_url)
        return {
            "path": dirpath,
            "fetch_url": fetch_url,
            "push_urls": push_urls,
            "ignore": False,
        }
    return None


def add_repo(dirpath, ignore):
    """Add a repo."""
    if not repo_exists(dirpath):
        repo_list.append(get_repo_info(dirpath))
        logmsg.success(f'"{dirpath}" successfully added')
        return repo_list
    logmsg.warning(f'"{dirpath}" already exists')
    return repo_list


def remove_repo(dirpath):
    """Remove a repo."""
    if repo_exists(dirpath):
        i = 0
        for r in repo_list:
            if r["path"] == dirpath:
                if logmsg.confirm(f'Are you sure you want to remove "{dirpath}"'):
                    del repo_list[i]
                    logmsg.success(f'"{dirpath}" successfully removed')
                return repo_list
            i += 1
    logmsg.warning(f'"{dirpath}" does not exist')
    return repo_list


def update_origins(repo_users):
    """Update all repo origins to SSH."""
    # logmsg.warning('This is in development and will not actually make any changes')
    for obj in repo_list:
        if any(user in obj["fetch_url"] for user in repo_users):
            # print(obj['fetch_url'])
            url = re.sub(r"https://(.+@)?|ssh://(.+@)?", "git@", obj["fetch_url"])
            url = re.sub(r"org/", "org:", url)
            url = re.sub(r"com/", "com:", url)
            if obj["fetch_url"] != url:
                print("-" * 40)
                print(obj["fetch_url"] + " ==> " + url)
                obj["fetch_url"] = url
                abs_path = os.path.expanduser(obj["path"])
                cmd = (
                    'cd "%s" && git remote rm origin || true && git remote add origin %s'
                    % (abs_path, url)
                )
                print(f"=> {cmd}")
                output = exec_cmd(cmd)
                # print(output.strip())


def set_upstream(dirpath):
    """Set upstream to current branch."""
    os.chdir(dirpath)
    output = exec_cmd("git branch")
    regex = r"\* (.+)"
    matches = re.search(regex, output)
    if matches:
        branch = matches.group(1)
    print("Upstream not set. Setting upstream ...")
    cmd = f"git branch --set-upstream-to=origin/{branch}"
    print(f"=> {cmd}")
    exec_cmd(cmd)


def run():
    """Run script."""
    global repo_list, exclude_list

    # Ensure proper command line usage
    args = get_parser().parse_args()

    # Read our config file
    config_file = os.path.expanduser("~/config/git-lazy.cfg")
    if os.path.exists(config_file):
        config = configparser.ConfigParser()
        config.read(config_file)
    else:
        logmsg.warning(f'"{config_file}" does not exist')
        exit()

    repo_file = os.path.expanduser(config.get("default", "repo_list"))
    search_dirs = config.get("default", "search_dirs").split(",")
    repo_users = config.get("default", "repo_users").split(",")

    exclude_list = get_dirs(config.get("default", "exclude_dirs").split(","))

    repo_list = read_file(repo_file)

    # Remove paths that no longer exist
    # for repo in repo_list:
    #     repo['path'] = platform_path(repo['path'])
    #     if not os.path.exists(repo['path']):
    #         # os.remove(repo['path'])
    #         print(f"remove {repo['path']}")
    # return

    if args.ask_ignore:
        ask_ignore(repo_users)
        write_file(repo_list, repo_file)
        exit()

    if args.update_origins:
        update_origins(repo_users)
        write_file(repo_list, repo_file)
        exit()

    if args.add:
        repo_list = add_repo(args.add, args.ignore)
        write_file(repo_list, repo_file)
        exit()

    if args.remove:
        repo_list = remove_repo(args.remove)
        write_file(repo_list, repo_file)
        exit()

    # Find repos in preset directories
    if args.find:
        start_time = time.time()
        search = get_dirs(search_dirs)

        for dirpath in search:
            repo_dirs = find_repos(dirpath)
            get_repo_list_info(repo_dirs, repo_users)

        write_file(repo_list, repo_file)
        elapsed_time = time.time() - start_time
        display_time = time.strftime("%H:%M:%S", time.gmtime(elapsed_time))
        logmsg.success(f"DONE in {display_time}!")

        exit()

    # Sync
    if args.sync:
        sync_repos()
        logmsg.success("DONE!")
        exit()

    # Do our git operation on all found git repos
    ignored = []
    for repo in repo_list:
        repo["path"] = platform_path(repo["path"])
        if "ignore" in repo and repo["ignore"] is True:
            # logmsg.warning(repo['path'] + ' is on the ignore list')
            ignored.append(repo["path"])
            continue
        if not os.path.exists(os.path.join(repo["path"], ".git")):
            logmsg.warning(repo["path"] + " is not a git repo")
            continue

        # print(repo['path'])
        os.chdir(repo["path"])

        # Set upstream to current branch
        # branch = exec_cmd("git branch | awk '{ print $2}'")
        # exec_cmd('git branch --set-upstream-to=origin/%s' % branch.strip())

        if args.method == "push":
            raise NotImplementedError("Do we want to do this?")
        else:
            output = exec_cmd("git " + args.method)

            if not output:
                set_upstream(repo["path"])
                continue
            if "Changes" in output or "Untracked" in output:
                print("-" * 80)
                logmsg.header(repo["path"])
                logmsg.info(output)

    logmsg.header("Ignored repos...")
    for repo in ignored:
        logmsg.warning(repo)


# -----------------------------------------------------------------------------

if __name__ == "__main__":
    run()
